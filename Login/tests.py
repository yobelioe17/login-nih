from django.test import TestCase
from django.conf import settings
from importlib import import_module
from .views import index
from django.http import HttpRequest
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
import time


# FOR SELENIUM
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class UnitTest(TestCase):

    def test_page_login(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)
    
    def test_page_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code,301)
    
    def test_page_template_logout(self):
        response = Client().get('/logout')
        self.assertEqual(response.status_code,301)

    def test_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'Base.html')
    
    def test_header(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Silakan Masuk",html_response)

class CommentFunctionalTest(LiveServerTestCase):

    
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(CommentFunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(CommentFunctionalTest, self).tearDown()

    def test_login_gagal(self):
        self.selenium.get(self.live_server_url + '/')
        self.assertIn("Login", self.selenium.title)
        username = self.selenium.find_element_by_id("id_username")
        username.send_keys("admin")
        password = self.selenium.find_element_by_id("id_password")
        password.send_keys("admin")
        kirim = self.selenium.find_element_by_id("submit")
        kirim.submit()
        response = Client().get('/')
        self.assertTemplateUsed(response,'Base.html')