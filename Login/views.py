from django.shortcuts import render, redirect
from .forms import FormMasuk
from django.contrib.auth import authenticate, login, logout


# Create your views here.

def index(request):
    form = FormMasuk()
    flag = False
    if request.method == "POST":
        form = FormMasuk(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password = password)
            if user is not None:
                login(request, user)
                flag = True
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/')
            else:
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/')
    else:
        response = {'form':form,'flag':flag}
        return render(request,'Base.html',response)


def logoutnih(request):
    logout(request)
    form = FormMasuk()
    response = {'form':form}
    return redirect('/')
