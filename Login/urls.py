from django.urls import include, path
from . import views

urlpatterns = [ 
    path('', views.index, name='awalan'),
    path('logout/', views.logoutnih, name='logout'),
]
