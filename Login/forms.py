from django import forms

class FormMasuk(forms.Form):

    username = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': "Username Anda",
        'required': True,
    }))
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        'type' : 'password',
        'placeholder': "Password Anda",
        'required': True,
    }))
